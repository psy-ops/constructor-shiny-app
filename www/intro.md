---
title: "Intro"
output: html_document
---

### Welcome to the Constructor Shiny App! <img src="psycore-homepage-image.svg" alt="The PsyCore logo" class="psycore-logo" />


This Shiny App allows you to import a DCT specification and edit it or to manually create one from scratch. The app will then produce a human-readable HTML representation, a machine-readable YAML representation, and it will allow you to submit your DCT specification to the [PsyCoRe.one](https://psycore.one) Psychological Construct Repository.

To read more about DCT specifications, please consult the preprint at [https://psyarxiv.com/8tpcv](https://psyarxiv.com/8tpcv).


