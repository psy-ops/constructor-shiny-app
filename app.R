###-----------------------------------------------------------------------------
### Check for the required packages
###-----------------------------------------------------------------------------

if (!requireNamespace("shiny"))
  stop("The {shiny} package is needed for creating Shiny apps.");
if (!requireNamespace("shinyjs"))
  stop("The {shinyjs} package is needed for enabling/disabling buttons.");
if (!requireNamespace("rclipboard"))
  stop("The {rclipboard} package is needed to enable filling the clipboard with a button");
if (!requireNamespace("git2r"))
  stop("The {git2r} package is needed for submitting to PsyCoRe.one.");
if (!requireNamespace("psyverse"))
  stop("The {psyverse} package is needed for creating DCTs.");

### Tell Shiny it needs to package the markdown package
invisible(markdown::markdown_options());
invisible(readxl::readxl_progress());

### For when doing parallel development
# devtools::load_all("B:/git/R/psyverse");
# devtools::install_gitlab("r-packages/psyverse@dev");

### To check package origins (in case some are loaded from e.g. GitLab):
# devtools::session_info()

###-----------------------------------------------------------------------------
### Settings
###-----------------------------------------------------------------------------

appVersion <- "0.1.0";

appTitle <- paste0("Constructor Shiny App (v", appVersion, ")");

repoPrefix <- "https://psycore.one/";

###-----------------------------------------------------------------------------
### User interface specification
###-----------------------------------------------------------------------------

ui <- shiny::fluidPage(
  
  title = appTitle,
  
  ### Theme
  theme = bslib::bs_theme(bootswatch = "pulse"),
  
  ### Add Shiny JS to disable/enable download buttons
  shinyjs::useShinyjs(),
  
  ### Use waiter
  waiter::use_waiter(),
  
  ### Use clipboard functionality
  rclipboard::rclipboardSetup(),
  
  ### Specify language
  lang = "en",
  
  ### Make the horizontal line more visible;
  shiny::tags$head(
    shiny::tags$style(
      shiny::HTML(
        "
        hr {
          border-top: 1px solid #000000
        };

        ul.pagination li.paginate_button {
          margin: 1px 3px;
        }
        
        .psycore-logo {
          width: 400px;
          float: right;
          margin-left: 30px;
        }
        
        "
      )
    )
  ),
  
  shiny::navbarPage(
    
    title = appTitle,
    id = "constructorTabSetPanel",
    
    ###---------------------------------------------------------------------------
    ### Intro tab
    ###---------------------------------------------------------------------------
    
    shiny::tabPanel(
      title = "Intro",
      value = "appIntroTab",
      icon = shiny::icon(name = "door-open",
                         lib = "font-awesome"),
      shiny::fluidRow(
        shiny::column(
          width = 12,
          shiny::includeMarkdown("www/intro.md")
        )
      ),
      
      waiter::waiter_show_on_load(color = "#333333")
    ),
    
    ###---------------------------------------------------------------------------
    ### Import tab
    ###---------------------------------------------------------------------------
    
    shiny::tabPanel(
      title = "Import",
      value = "importTab",
      icon = shiny::icon(name = "right-to-bracket",
                         lib = "font-awesome"),
      
      shiny::fluidPage(

        shiny::fluidRow(
          shiny::column(
            width = 12,
            shiny::h3(
              "Import a DCT specification"
            ),
            shiny::p(
              "Here, you can import a DCT specification. ",
              "You can copy-paste a link to a Google spreadsheet ",
              "(in that case, ensure it can be viewed publicly), upload ",
              "a spreadsheet in Office Open XML format (.xlsx), or ",
              "upload a DCT specification in the DCT YAML standard."
            ),
            shiny::p(
              "Once imported, you can edit the contents in the \"Edit\" ",
              "tab, or view them in the \"View\" and \"YAML\" tabs. If ",
              "you're certain this version is final, you can submit it to ",
              shiny::a(
                "PsyCoRe.one",
                href = "https://psycore.one",
                target = "_blank"
              ),
              ".",
              shiny::uiOutput("exampleSheet_clipboard_link")
            )
          )
        ),
        
        shiny::fluidRow(
          shiny::column(
            width = 12,
            shiny::textInput(
              inputId = "gSheetsURL",
              label = NULL, # "Google Sheets URL",
              value = "",
              width = "100%",
              placeholder = "Type or paste a Google Sheets URL here"
            )
          )
        ),
        shiny::fluidRow(
          shiny::column(
            width = 12,
            shiny::fileInput(
              inputId = "uploadFile",
              label = NULL, # "Upload a file",
              accept = c(".csv", ".yaml", ".yml", ".xlsx"),
              width = "100%",
              placeholder = "Upload a .dct.yaml, .csv, or .xlsx file"
            )
          )
        ),
        shiny::fluidRow(
          shiny::column(
            width = 12,
            shiny::p(
              shiny::textOutput(
                outputId = "importMessage"
              )
            )
          )
        )
      )
    ),  ### End tabpanel
    
    ###---------------------------------------------------------------------------
    ### Edit tab
    ###---------------------------------------------------------------------------
    
    shiny::tabPanel(
      title = "Edit",
      value = "editTab",
      icon = shiny::icon(name = "pen-nib",
                         lib = "font-awesome"),
      
      shiny::fluidPage(

        shiny::fluidRow(
          shiny::column(
            width = 12,
            shiny::h3(
              "Import a DCT specification"
            ),
            shiny::p(
              "Here, you can edit a DCT specification. ",
              "When you are done, you can view the results ",
              "in he \"View\" and \"YAML\" tabs. If ",
              "you're certain this version is final, you can submit it to ",
              shiny::a(
                "PsyCoRe.one",
                href = "https://psycore.one",
                target = "_blank"
              ),
              "."
            )
          )
        ),
        
        shiny::fluidRow(
          shiny::column(
            width = 6,
            shiny::textInput(
              inputId = "label",
              label = "Construct label",
              value = "",
              width = "100%",
              placeholder = "Type or paste the construct label here"
            )
          ),
          shiny::column(
            width = 6,
            shiny::textInput(
              inputId = "prefix",
              label = "UCID prefix",
              value = "",
              width = "100%",
              placeholder = "Type or paste the UCID prefix here"
            )
          )
        ),
        
        shiny::fluidRow(
          shiny::column(
            width = 12,
            shiny::textAreaInput(
              inputId = "definition",
              label = "Construct definition",
              value = "",
              placeholder = "Type or paste the construct definition here",
              width = "900px",
              rows = "3",
              resize = "both"
            )
          )
        ),

        shiny::fluidRow(
          shiny::column(
            width = 12,
            shiny::textAreaInput(
              inputId = "measureDev",
              label = "Instruction for developing measurement instruments",
              value = "",
              placeholder = "Type or paste the instruction for developing measurement instruments here",
              width = "900px",
              rows = "3",
              resize = "both"
            )
          )
        ),
        
        shiny::fluidRow(
          shiny::column(
            width = 12,
            shiny::textAreaInput(
              inputId = "measureCode",
              label = "Instruction for coding measurement instruments",
              value = "",
              placeholder = "Type or paste the instruction for coding measurement instruments here",
              width = "900px",
              rows = "3",
              resize = "both"
            )
          )
        ),
        
        shiny::fluidRow(
          shiny::column(
            width = 12,
            shiny::textAreaInput(
              inputId = "aspectDev",
              label = "Instruction for eliciting qualitative data",
              value = "",
              placeholder = "Type or paste the instruction for eliciting qualitative data here",
              width = "900px",
              rows = "3",
              resize = "both"
            )
          )
        ),
        
        shiny::fluidRow(
          shiny::column(
            width = 12,
            shiny::textAreaInput(
              inputId = "aspectCode",
              label = "Instruction for coding qualitative data",
              value = "",
              placeholder = "Type or paste the instruction for coding qualitative data here",
              width = "900px",
              rows = "3",
              resize = "both"
            )
          )
        ),
        
        shiny::fluidRow(
          shiny::column(
            width = 12,
            shiny::textAreaInput(
              inputId = "comments",
              label = "Comments",
              value = "",
              placeholder = "Here you can add comments, for example about who you are",
              width = "900px",
              rows = "2",
              resize = "both"
            )
          )
        )
      )
    ),  ### End tabpanel

    ###-----------------------------------------------------------------------
    
    shiny::tabPanel(
      title = "View",
      value = "viewTab",
      icon = shiny::icon(name = "eye",
                         lib = "font-awesome"),
      
      shiny::fluidPage(
        
        shiny::fluidRow(
          shiny::column(
            width = 10,
            shiny::h3(
              "View and download a DCT specification in HTML"
            ),
            shiny::p(
              "Here, you can view (and download) a human-readable ",
              "representation DCT specification in HTMl format."
            )
          ),
          shiny::column(
            width = 2,
            align = "right",
            shinyjs::disabled(
              shiny::downloadButton("downloadDCT_fromView",
                                    "Download",
                                    class = "btn-success")
            )
          ),
          shiny::hr()
        ),

        shiny::fluidRow(
          shiny::column(
            width = 12,
            shiny::p(
              shiny::textOutput(
                outputId = "htmlViewMessage"
              )
            )
          )
        ),
        
        shiny::fluidRow(
          shiny::column(
            width = 12,
            shiny::htmlOutput(
              outputId = "viewDCTspec"
            )
          )
        )        
      )
    ),  ### End tabpanel
      
    ###-----------------------------------------------------------------------
    
    shiny::tabPanel(
      title = "YAML",
      value = "yamlTab",
      icon = shiny::icon(name = "code",
                         lib = "font-awesome"),
      
      shiny::fluidPage(
        
        shiny::fluidRow(
          shiny::column(
            width = 9,
            shiny::h3(
              "View and download a DCT specification as YAML"
            ),
            shiny::p(
              "Here, you can view (and download) a machine-readable ",
              "representation DCT specification following the YAML standard."
            )
          ),
          shiny::column(
            width = 3,
            align = "center",
            shiny::p(
              shinyjs::disabled(
                shiny::downloadButton("downloadDCT_fromYAML",
                                      "Download",
                                      class = "btn-success")
              )
            ),
            shiny::p(
              shiny::uiOutput("YAML_clipboard_button")
            )
          ),
          shiny::hr()
        ),
        
        shiny::fluidRow(
          shiny::column(
            width = 12,
            shiny::p(
              shiny::textOutput(
                outputId = "yamlViewMessage"
              )
            )
          )
        ),
        
        shiny::fluidRow(
          shiny::column(
            width = 12,
            shiny::verbatimTextOutput(
              outputId = "yaml_DCT_output"
            )
          )
        )
      )
    ), ### End tabpanel

    shiny::tabPanel(
      title = "Submit",
      value = "submitTab",
      icon = shiny::icon(name = "code-compare",
                         lib = "font-awesome"),
      shiny::fluidPage(
        
        shiny::fluidRow(
          shiny::column(
            width = 12,
            shiny::h3(
              "Submit your DCT specification"
            ),
            shiny::p(
              "Here, you can submit your DCT specification to the ",
              "PsyCoRe.one repository."
            ),
            shiny::p(
              "Please make sure you consider this the final version before ",
              "you submit it."
            ),
            shiny::h3(
              "Notification: this app is presently broken; there is a problem ",
              "with the way we use the 'gitr' package... You can download the ",
              "YAML file that was created and send it to Gjalt-Jorn or Rik to ",
              "get it added to PsyCoRe (or use git yourself if you want)."
            ),
            shiny::hr()
          ),
        ),

        shiny::fluidRow(
          shiny::column(
            width = 12,
            shiny::uiOutput(
              outputId = "submitMessage"
            )
          )
        ),
        shiny::fluidRow(
          shiny::column(
            width = 12,
            shiny::p(
              shinyjs::disabled(
                shiny::actionButton(
                  inputId = "submitButton",
                  label = "Submit to PsyCoRe.one",
                  icon = shiny::icon(name = "code-compare",
                                     lib = "font-awesome")
                )
              )
            )
          )
        )
      )
    )  ### End tabpanel

    ###-----------------------------------------------------------------------
    
  ) ### End navbarpage
  
);

###-----------------------------------------------------------------------------
### Shiny functionality specification
###-----------------------------------------------------------------------------

server <- function(input, output, session) {

  ###---------------------------------------------------------------------------
  ### Waiter
  ###---------------------------------------------------------------------------
  
  shiny::observe(waiter::waiter_hide());
  
  ###---------------------------------------------------------------------------
  ### Interface stuff
  ###---------------------------------------------------------------------------

  output$submitMessage <- shiny::renderUI({
    shiny::p(
      paste0(
        "Once you imported or manually specified a DCT specification, ",
        "you can submit it to PsyCoRe.one here."
      )
    );
  });
  
  output$htmlViewMessage <- shiny::renderText({
    paste0(
      "Once you imported or manually specified a DCT specification, ",
      "you can view it here."
    );
  });  
  
  output$yamlViewMessage <- shiny::renderText({
    paste0(
      "Once you imported or manually specified a DCT specification, ",
      "you can view it here."
    );
  });  
  
  output$YAML_clipboard_button <-
    shiny::renderUI(
      rclipboard::rclipButton(
      inputId = "clipbtn",
      label = "Copy to clipboard",
      clipText = "", 
      icon = icon("clipboard"),
      disabled = ""
      )
  );
  
  output$exampleSheet_clipboard_link <-
    shiny::renderUI(
      shiny::p(
        "If you just want to try out the app, ",
        "you can use the example DCT specification in ",
        shiny::a(
          "this google sheet",
          href = "https://docs.google.com/spreadsheets/d/13xCMZEGFSzmLkAVsVzjan4LzGsehx2FwOwyzhn15bL0",
          target = "_blank"
        ),
        ": ",
        rclipboard::rclipButton(
          inputId = "exampleSheetClipboardLink",
          label = "copy to clipboard",
          clipText = "https://docs.google.com/spreadsheets/d/13xCMZEGFSzmLkAVsVzjan4LzGsehx2FwOwyzhn15bL0",
          class = "btn-xs"
        )
      )
    );
  
  ###---------------------------------------------------------------------------
  ### Reactive objects
  ###---------------------------------------------------------------------------
  
  ### Create reactive object with UCID
  ucid <- shiny::reactiveVal();
  
  ### Full DCT specification
  dctSpec <- shiny::reactiveVal();
  
  ### HTML DCT specification
  htmlSpec <- shiny::reactiveVal();
  
  ### As YAML
  yamlSpec <- shiny::reactiveVal();
  
  ### Combine input objects
  ### https://stackoverflow.com/questions/41960953/how-to-listen-for-more-than-one-event-expression-within-a-shiny-observeevent
  
  manualInput <- shiny::reactive({
    list(
      input$prefix,
      input$label,
      input$definition,
      input$measureDev,
      input$measureCode,
      input$aspectDev,
      input$aspectCode,
      input$comments
    );
  });
  
  ###---------------------------------------------------------------------------
  ### Import google sheet
  ###---------------------------------------------------------------------------
  
  shiny::observeEvent(input$gSheetsURL, {
    
    req(input$gSheetsURL);
    
    importedDCT <-
      tryCatch(
        psyverse::dct_from_spreadsheet(
          input$gSheetsURL,
          sheet = 1
        ),
        error = function(e) {
          res <- e$message;
          class(res) <- "errorMessage";
          return(res);
        }
      );
    
    if (inherits(importedDCT, "errorMessage")) {
      output$importMessage <- shiny::renderText({
        paste0(
          "The URL you pasted in the field for a Google Sheets URL does not ",
          "appear to be a valid URL to a Google Sheet containing a valid ",
          "DCT specification. Please check that the first worksheet ",
          "contains one column labelled \"field\" and one column labelled ",
          "\"content\", and that you specified at least content for the ",
          "fields \"label\" and \"definition\". The error I saw was:\n\n",
          importedDCT
        );
      });
    } else {
      dctSpec(importedDCT$dcts[[1]]);
      output$importMessage <- shiny::renderText({
        paste0(
          "I read the first worksheet from the spreadsheet you provided; ",
          "you can now view this in the other tabs."
        );
      });
    }

  });
  
  ###---------------------------------------------------------------------------
  ### Import a file
  ###---------------------------------------------------------------------------
  
  shiny::observeEvent(input$uploadFile, {
    
    req(input$uploadFile);
    
    fileName <- input$uploadFile$name;
    filePath <- input$uploadFile$datapath;
    
    if (grepl("yaml|yml", fileName, ignore.case = TRUE)) {
      importedDCT <-
        psyverse::load_dct_specs(
          file = input$uploadFile$datapath
        );
      dctSpec(
        importedDCT$input$dctSpecs[[1]]$dct
      );
    } else if (grepl("csv|xls", fileName, ignore.case = TRUE)) {
      importedDCT <-
        psyverse::dct_from_spreadsheet(
          input$uploadFile$datapath,
          sheet = 1
        );
      dctSpec(importedDCT$dcts[[1]]);
    }
    
  });
  
  ###---------------------------------------------------------------------------
  ### Populate 'edit' fields when importing
  ###---------------------------------------------------------------------------
  
  shiny::observeEvent(dctSpec(), {
    
    importedDCT <- dctSpec();
    
    shiny::updateTextInput(
      inputId = "prefix",
      value = gsub("_[a-zA-Z0-9]*$",
                   "",
                   importedDCT$id)
    );
    
    shiny::updateTextInput(inputId = "label",
                           value = importedDCT$label);
    
    shiny::updateTextAreaInput(inputId = "definition",
                               value = importedDCT$definition$definition);
    shiny::updateTextAreaInput(inputId = "measureDev",
                               value = importedDCT$measure_dev$instruction);
    shiny::updateTextAreaInput(inputId = "measureCode",
                               value = importedDCT$measure_code$instruction);
    shiny::updateTextAreaInput(inputId = "aspectDev",
                               value = importedDCT$aspect_dev$instruction);
    shiny::updateTextAreaInput(inputId = "aspectCode",
                               value = importedDCT$aspect_code$instruction);
    shiny::updateTextAreaInput(inputId = "comments",
                               value = importedDCT$comments);
    
    htmlSpec(
      psyverse::dct_object_to_html(
        dctSpec(),
        urlPrefix = "https://psycore.one/",
        collapseButtons = FALSE
      )
    );
    
    yamlSpec(
      paste0("---\n",
             psyverse::dct_object_to_yaml(
              dctSpec()
             ),
             "\n---\n")
    );
    
    shinyjs::enable("downloadDCT_fromView");
    shinyjs::enable("downloadDCT_fromYAML");
    shinyjs::enable("submitButton");
    
    ### Create clipboard buttons using reactive value
    output$YAML_clipboard_button <-
      shiny::renderUI(
        rclipboard::rclipButton(
          inputId = "clipbtn",
          label = "Copy to clipboard",
          clipText = paste0(yamlSpec(), collapse="\n"), 
          icon = icon("clipboard")
        )
      );

    output$viewDCTspec <- shiny::renderUI(
      shiny::p(
        shiny::HTML(
          htmlSpec()
        )
      )
    );
    
    output$yaml_DCT_output <- shiny::renderText(
      yamlSpec()
    );
    
    output$htmlViewMessage <- shiny::renderText({
      ""
    });
    
    output$yamlViewMessage <- shiny::renderText({
      ""
    });
    
    output$submitMessage <- shiny::renderUI({
      ""
    });
    
  });
  
  
  ###---------------------------------------------------------------------------
  ### Create DCT object
  ###---------------------------------------------------------------------------
  
  shiny::observeEvent(manualInput(), {
    
    shiny::req(input$prefix,
               input$label,
               input$definition,
               input$measureDev,
               input$measureCode,
               input$aspectDev,
               input$aspectCode);

    if (nchar(input$prefix) > 0) {
      
      ### Parse sources and store the result in the
      ### reactive `parsedSourcesObject` object
      ucid(
        psyverse::generate_id(
          prefix = input$prefix
        )
      );

      dctSpec(
        psyverse::dct_object(
          id = ucid(),
          label = input$label,
          definition = input$definition,
          measure_dev = input$measureDev,
          measure_code = input$measureCode,
          aspect_dev = input$aspectDev,
          aspect_code= input$aspectCode,
          comments = input$comments
        )
      );

    } else {

      shinyjs::disable("downloadDCT_fromView");
      shinyjs::disable("downloadDCT_fromYAML");
      shinyjs::disable("submitButton");
      
      output$YAML_clipboard_button <-
        shiny::renderUI(
          rclipboard::rclipButton(
            inputId = "clipbtn",
            label = "Copy to clipboard",
            clipText = "", 
            icon = icon("clipboard"),
            disabled = ""
          )
        );
      
    }
    
  });
  
  ###---------------------------------------------------------------------------
  ### Download the DCT specification as HTML
  ###---------------------------------------------------------------------------
  
  output$downloadDCT_fromView <- shiny::downloadHandler(
    filename = function() {
      paste0(ucid(), ".html")
    },
    content = function(file) {
      
      res <- dctSpec();
      
      res <- paste0("<!doctype html>
<html lang=en>
<head>
<meta charset=utf-8>
<title>",
paste0(res$label, " ", res$id),
"</title>
<style></style>
</head>
<body>
", paste0(htmlSpec(), collapse="\n"), "
</body>
</html>");
      
      writeLines(
        res,
        file
      );
    }
  );
  
  ###---------------------------------------------------------------------------
  ### Download the DCT specification as YAML
  ###---------------------------------------------------------------------------
  
  output$downloadDCT_fromYAML <- shiny::downloadHandler(
    filename = function() {
      paste0(ucid(), ".dct.yaml")
    },
    content = function(file) {
      psyverse::save_to_yaml(
        dctSpec(),
        file = file
      )
    }
  );  
  
  ###---------------------------------------------------------------------------
  ### Submit the DCT specification to PsyCoRe.one
  ###---------------------------------------------------------------------------
  
  shiny::observeEvent(input$submitButton, {

    # Sys.getenv("PSYCORE_CONSTRUCTOR_GITLAB_PAT");
    # Sys.setenv(PSYCORE_CONSTRUCTOR_GITLAB_PAT = "token");

    cred <- git2r::cred_token(token = "PSYCORE_CONSTRUCTOR_GITLAB_PAT");
    
    tempPath <- tempfile(pattern = "git2r-");
    tempRepo <- git2r::clone("https://gitlab.com/psy-ops/psycore-one.git",
                             tempPath);
    tempFile <-
      file.path(tempPath, "data", "dctspecs", paste0(ucid(), ".dct.yaml"));

    git2r::config(tempRepo,
                  user.name = "PsyCoRe Constructor",
                  user.email = "constructor_shiny_app@psycore.one",
                  http.followRedirects = "true"); ### Because of error in
                                                  ### Shiny logs:
    ### Error in 'git2r_push': config value 'http.followRedirects' was not found

    psyverse::save_to_yaml(
      dctSpec(),
      file = tempFile
    );

    git2r::add(
      tempRepo,
      path = tempFile
    );
    
    git2r::commit(
      tempRepo,
      message = paste("Submission of DCT specification with UCID ", ucid())
    );
    
    git2r::push(
      object = tempRepo,
      #name = "origin",
      #refspec = "refs/heads/master",
      credentials = cred
    );
    
    output$submitMessage <- shiny::renderUI({
      shiny::p(
        "Done submitting! Your new DCT will soon be available at ",
        shiny::a(paste0(repoPrefix, ucid()),
                 href = paste0(repoPrefix, ucid())),
        "."
      );
    });
        
  });
  
}

shiny::shinyApp(ui, server);
